package ru.nsu.fit.endpoint.rest;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Roles {
    public static final String ADMIN = "ADMIN";
    public static final String UNKNOWN = "UNKNOWN";
}
